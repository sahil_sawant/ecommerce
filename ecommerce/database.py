import sqlite3

conn = sqlite3.connect('product.db')
c = conn.cursor()

def create_table():
    c.execute('CREATE TABLE IF NOT EXISTS items(Price REAL, Seller_Info TEXT, Buyer_ratings REAL, Eta TEXT)')
    conn.commit()

def data_entry():
    c.execute("INSERT INTO items VALUES(500, 'BG Sellers', 4, '4 days')")
    c.close()
    conn.close()

create_table()
data_entry()