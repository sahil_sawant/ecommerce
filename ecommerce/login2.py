username = "user"
word = "0000"
userid = ""
password = ""
loginLimit = 3
count = int(0)
limit = False
class login:
    messages = {
        "user" : "Hello User",
        "invalid" : "You are not a User",
        "try" : "try again"
    }
    while userid != username and not (limit) :
        if count <= loginLimit :
            userid = input("Enter your user ID: ")

            count += 1
            if userid != username and count <= loginLimit:
                print(messages["try"])
        else :
            limit = True

    while not (limit) and password != word:
        if count <= loginLimit :

            password = input("Enter your password: ")
            count += 1
            if password != word and count <= loginLimit:
                print(messages["try"])
        else :
            limit = True


    if limit:
        print(messages["invalid"])
        exit()
    else:
        print(messages["user"])
